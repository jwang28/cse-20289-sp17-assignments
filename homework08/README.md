Homework 08
===========
Activity 1:
1. I allocated an memory with the size of Entry and a memory for the size of 
the key and another one for value if value is a string. I deallocated the 
key and the value if it is a string and also the Entry itself. If recursive 
is called, I first keep track of the next Entry then delete the key, value 
(if needed), and the Entry itself then move on to the next Entry.
2. I allocated memory with the size of Map and allocated memory for the 
array of buckets with the size of the Entry and with the number of the 
capacity. In delete, I deallocated the memory assigned to the buckets and 
the map.
3. I allocated a new memory called buckets with size of the new_capacity and 
relinked all the entries in the old bucket to the new bucket and set the new 
bucket to replace the old bucket.
4. It first checks if the map needs to be resized and resize it if needed. 
It then search for the key in the map. If the key is in the map, the entry 
will be updated, if it's not, then a new entry will be created and inserted 
to the linked list.
ber of the 
capacity. In delete, I deallocated the memory assigned to the buckets and 
the map.
3. I allocated a new memory called buckets with size of the new_capacity and 
relinked all the entries in the old bucket to the new bucket and set the new 
bucket to replace the old bucket.
4. It first checks if the map needs to be resized and resize it if needed. 
It then search for the key in the map. If the key is in the map, the entry 
will be updated, if it's not, then a new entry will be created and inserted 
to the linked list.
time: average O(1) worst O(1)
space: average O(1) worst O(1)
5. it first hashes the key and locate the bucket the key should be in. It 
then go through the entries in the bucket and check if the key is in the 
bucket.
time: average O(alpha) worst O(n)
space: average O(1) worst O(1)
6. It first searches for the key. If the key is in the map, it then locates 
it by hashing the value and goes through the map again to find the previous 
entry and connects the previous entry with the next entry before deleting 
the entry.
time: average O(alpha) worst O(n)
space: average O(1) worst O(1)

Activity 2:
| NITEMS   | ALPHA    | TIME      | SPACE       |
| -------- | -------- |-----------|-------------|
| 1        | 0.5      | 0.000999  | 0.574219    |
| 1        | 0.75     | 0.000999  | 0.578125    |
| 1        | 0.9      | 0.000999  | 0.578125    |
| 1        | 1.0      | 0.000999  | 0.574219    |
| 1        | 2.0      | 0.000999  | 0.578125    |
| 1        | 8.0      | 0.000999  | 0.570312    |
| 1        | 16.0     | 0.000999  | 0.578125    |
| 10       | 0.5      | 0.000999  | 0.574219    |
| 10       | 0.75     | 0.000999  | 0.578125    |
| 10       | 0.9      | 0.000999  | 0.578125    |
| 10       | 1.0      | 0.000999  | 0.578125    |
| 10       | 2.0      | 0.000999  | 0.574219    |
| 10       | 8.0      | 0.000999  | 0.578125    |
| 10       | 16.0     | 0.000999  | 0.578125    |
| 100      | 0.5      | 0.000999  | 0.578125    |
| 100      | 0.75     | 0.000999  | 0.582031    |
| 100      | 0.9      | 0.000999  | 0.582031    |
| 100      | 1.0      | 0.000999  | 0.578125    |
| 100      | 2.0      | 0.000999  | 0.578125    |
| 100      | 8.0      | 0.000999  | 0.582031    |
| 100      | 16.0     | 0.000999  | 0.582031    |
| 1000     | 0.5      | 0.001998  | 0.683594    |
| 1000     | 0.75     | 0.001998  | 0.699219    |
| 1000     | 0.9      | 0.001999  | 0.703125    |
| 1000     | 1.0      | 0.000999  | 0.656250    |
| 1000     | 2.0      | 0.001998  | 0.656250    |
| 1000     | 8.0      | 0.000999  | 0.656250    |
| 1000     | 16.0     | 0.001998  | 0.656250    |
| 10000    | 0.5      | 0.012998  | 2.632812    |
| 10000    | 0.75     | 0.011997  | 1.812500    |
| 10000    | 0.9      | 0.011997  | 1.824219    |
| 10000    | 1.0      | 0.011997  | 1.882812    |
| 10000    | 2.0      | 0.011998  | 1.566406    |
| 10000    | 8.0      | 0.011998  | 1.375000    |
| 10000    | 16.0     | 0.010998  | 1.343750    |
| 100000   | 0.5      | 0.155976  | 17.503906   |
| 100000   | 0.75     | 0.188970  | 20.015625   |
| 100000   | 0.9      | 0.142978  | 12.183594   |
| 100000   | 1.0      | 0.144977  | 12.179688   |
| 100000   | 2.0      | 0.140978  | 10.183594   |
| 100000   | 8.0      | 0.162974  | 8.683594    |
| 100000   | 16.0     | 0.206967  | 8.429688    |
| 1000000  | 0.5      | 1.676744  | 140.843750  |
| 1000000  | 0.75     | 1.847718  | 156.511719  |
| 1000000  | 0.9      | 1.940704  | 168.511719  |
| 1000000  | 1.0      | 1.682743  | 108.839844  |
| 1000000  | 2.0      | 1.759732  | 92.847656   |
| 1000000  | 8.0      | 2.488620  | 80.843750   |
| 1000000  | 16.0     | 3.510465  | 78.847656   |
| 10000000 | 0.5      | 21.432739 | 2176.507812 |
| 10000000 | 0.75     | 21.046799 | 1275.488281 |
| 10000000 | 0.9      | 21.660706 | 1344.507812 |
| 10000000 | 1.0      | 20.993807 | 1408.507812 |
| 10000000 | 2.0      | 23.570415 | 1024.507812 |
| 10000000 | 8.0      | 34.197800 | 827.488281  |
| 10000000 | 16.0     | 48.277660 | 795.488281  |
1. As load factor become greater, the program takes more time but less space. It does not 
surprise me because big load factors means less resizing therefore takes less space but it 
also becomes more difficult to search because there could be multiple items in a single 
bucket.
2. hash table has fast amortized operations and exhibit better cache behavior. Treap takes up 
less space and it has no over-allocation.
I will use a hash table because it is more straight-forward and more easy to understand and 
implement. 
