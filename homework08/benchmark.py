#!/usr/bin/env python2.7
import subprocess

print '| NITEMS   | ALPHA    | TIME      | SPACE       |'
print '| -------- | -------- |-----------|-------------|'
output = '| {:<8} | {:8} | {:9} | {:11} |'
num = 1
while (num <= 10000000):
	for lf in [0.5, 0.75, 0.9, 1.0, 2.0, 8.0, 16.0]:
		
		shuf_args = ["shuf", "-i1-" + str(num), "-n", str(num)]
		measure_args = ["./measure", "./freq", "-l", str(lf)]
		
		shuf_pipe = subprocess.Popen(shuf_args, stdout=subprocess.PIPE, 
		bufsize =-1).stdout
		
		measure_pipe = subprocess.Popen(measure_args, stdin=shuf_pipe, 
		stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=-1)
		
		shuf_pipe.close()
		
		line = measure_pipe.communicate()[1].split()
		
		print output.format(str(num), str(lf), line[0], line[2])
		
	num *= 10
	

