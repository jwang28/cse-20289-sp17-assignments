homework08 - Grading
===================

**Score**: 14.25/15

Deductions
----------
-0.25 - insert is O(n) worst case time and space
-0.25 - search is O(1) average time
-0.25 - remove is O(1) average time


Comments
--------
