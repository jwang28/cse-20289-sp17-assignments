/* map.c: separate chaining hash table */

#include "map.h"

/**
 * Create map data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @param   load_factor     Maximum load factor before resizing.
 * @return  Allocated Map structure.
 */
Map *	        map_create(size_t capacity, double load_factor) {
    Map *m = calloc(1, sizeof(Map));
    if (m) {
    	if (capacity == 0) {
 		   	m->capacity = DEFAULT_CAPACITY; 
	    } else {
    		m->capacity = capacity;
   		}
   		if (load_factor > 0) {
    		m->load_factor = load_factor;
    	} else {
    		m->load_factor = DEFAULT_LOAD_FACTOR;
    	}
    	m->size = 0;
    	m->buckets = calloc(m->capacity, sizeof(Entry));
    	return m;
    }
    return NULL;
}

/**
 * Delete map data structure.
 * @param   m               Map data structure.
 * @return  NULL.
 */
Map *	        map_delete(Map *m) {

    for (int i = 0; i < m->capacity; ++i){
        entry_delete(m->buckets[i].next, true);
    }

    free(m->buckets);
    free(m);
    return NULL;
}

/**
 * Insert or update entry into map data structure.
 * @param   m               Map data structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   type            Entry's value's type.
 */
void            map_insert(Map *m, const char *key, const Value value, Type type) {
	if (((double)m->size / m->capacity) > m->load_factor) {
		map_resize(m, (m->capacity)*2);
	}
	Entry *e = map_search(m, key);
	if (e) {
		entry_update(e, value, type); 
	} else {
	    uint64_t hash = fnv_hash(key, strlen(key)) % m->capacity;
		Entry *first = m->buckets[hash].next;
		m->buckets[hash].next = entry_create(key, value, first, type);
		m->size++;
	}
}

/**
 * Search map data structure by key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to search for.
 * @param   Pointer to the entry with the specified key (or NULL if not found).
 */
Entry *         map_search(Map *m, const char *key) {
    uint64_t hash = fnv_hash(key, strlen(key)) % m->capacity;
    Entry *e = m->buckets[hash].next;
    while (e) {
    	if (strcmp(e->key, key) == 0) {
    		return e;
    	}
    	e = e->next;
    }
/*    for (int i = 0; i < m->size; i++) {
    	if (strcmp(&(m->buckets[i])->key,key) == 0) {
    		Entry *e = &m->buckets[i];
    		return e;
    	}
    }*/
    return NULL;
}

/**
 * Remove entry from map data structure with specified key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to remove.
 * return   Whether or not the removal was successful.
 */
bool            map_remove(Map *m, const char *key) {
    Entry *e = map_search(m, key);
    if (e) {
        uint64_t hash = fnv_hash(key, strlen(key)) % m->capacity;
    	Entry *prev = &m->buckets[hash];
    	Entry *curr = prev->next;
    	while (curr) {
    		if (strcmp(curr->key, key) == 0) {
    			prev->next = curr->next;
    		    entry_delete(curr, false);
    		    m->size--;	
	        	return true;
    		}
    		curr = curr->next;
    		prev = prev->next;
    	}
    }
    return false;
}

/**
 * Dump all the entries in the map data structure.
 * @param   m               Map data structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void		map_dump(Map *m, FILE *stream, const DumpMode mode) {
	for (int i = 0; i < m->capacity; i++) {
		Entry *e = m->buckets[i].next;
                while(e){
		    entry_dump(e, stream, mode);
                    e = e->next;
                }
	}
}

/**
 * Resize the map data structure.
 * @param   m               Map data structure.
 * @param   new_capacity    New capacity for the map data structure.
 */
void            map_resize(Map *m, size_t new_capacity) {
	Entry *buckets = calloc(new_capacity, sizeof(Entry));
	for (int i = 0; i < m->capacity; i++) {
		Entry *e = m->buckets[i].next;
                Entry *temp;
		while (e) {
                        temp = e->next;
			uint64_t hash = fnv_hash(e->key, strlen(e->key)) % new_capacity;
			e->next =  buckets[hash].next;
			buckets[hash].next = e;
                        e = temp;
		}
	}
	free(m->buckets);
	m->buckets = buckets;
	m->capacity = new_capacity;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
