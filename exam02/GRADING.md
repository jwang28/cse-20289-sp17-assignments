GRADING - Exam 02
=================

- Identification:   2.75
- Web Scraping:     3.75
- Generators:       6
- Concurrency:      6
- Translation:      6
- Total:	    24.5

Comments
--------

Great job!
