#!/usr/bin/env python2.7

import os

result = set()

for line in os.popen('ps aux'):
	field = line.rstrip().split()[0]
	result.add(field)

print len(result)
