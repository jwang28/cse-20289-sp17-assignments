#!/usr/bin/env python2.7
import requests
import re

url = 'http://yld.me/aJt?raw=1'
text = requests.get(url).text.rstrip().split('\n')
result = []

for line in text:
	search = re.findall('([0-9]{3}-[0-9]{3}-[0-9]{3}[13579])', line)
	if search:
		result.append(search[0])

for item in result:
	print item
