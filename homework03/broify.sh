#!/bin/sh

if [ "$1" = "-h" ]; then
	echo "Usage: broify.sh"
	echo
	echo "  -d DELIM    Use this as the comment delimiter."
	echo "  -W          Don't strip empty lines."
	exit 0
fi

COMMENT=#
SPACE=no
while [ $# -gt 0 ]
do
	case $1 in
		-d) COMMENT=$2 shift;;
		-W) SPACE=yes;;
	esac
	shift
done

if [ $SPACE = no ]; then
	sed "s:$COMMENT.*::" | sed -r "s/[\t ]*$//" | sed -r "/^[\t ]*$/d"
else
	sed "s:$COMMENT.*::" | sed -r "s/[\t ]*$//"
fi

