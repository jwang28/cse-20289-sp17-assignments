#!/bin/sh

KEY=13

if [ "$#" -eq "1" ]; then
	if [ "$1" = "-h" ]; then
		echo "Usage: caesar.sh [rotation]"
		echo
		echo "This program will read from stdin and rotate (shift right) the text by the specified rotation. If none is specified, then the default value is 13."
		exit 0
	else
		KEY=$(($1%26))
	fi
fi

LOWER=abcdefghijklmnopqrstuvwxyz
FIRST=$(echo $LOWER | cut -c 1-$KEY)
SECOND=$(echo $LOWER | cut -c $(($KEY+1))-52)
NEWLOWER="$SECOND$FIRST"
NEWUPPER=$(echo "$NEWLOWER" | tr [:lower:] [:upper:])

tr a-zA-Z "$NEWLOWER$NEWUPPER"



#FIRST=$(echo {a..zA..Z} | cut -c 1-$KEY)
#SECOND=$(echo {a..zA..Z} | cut -c $(($KEY+1))-52)
#NEW="$SECOND$FIRST"
#tr a-zA-Z "$NEW"

