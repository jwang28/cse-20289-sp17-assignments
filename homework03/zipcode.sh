#!/bin/sh
usage(){
	echo 'Usage: zipcode.sh'
	echo
	echo '-c      CITY    Which city to search'
	echo '-f      FORMAT  Which format (text, csv)'
	echo '-s      STATE   Which state to search (Indiana)\n'
	echo 'If not CITY is specified, then all the zip codes for the STATE are displayed.'
	exit 0
}
CITY='.*'
STATE=INDIANA
while [ "$#" -gt "0" ]
do
	case $1 in
    	-c)  CITY=$2  shift;;
        -f)  FORMAT=$2  shift ;;
        -s)  STATE=`echo $2 | sed 's_ _%20_'` shift ;;
        -h)  usage;;
    esac 
    shift
done     
                                                                                                   
if [ "$FORMAT" = "text" ] || [ "$#" = "0" ] ; then
	curl -s http://www.zipcodestogo.com/$STATE/ | grep "href\=\"http\:\/\/www\.zipcodestogo\.com\/${CITY}\/" | grep -E '[0-9]{5}' -o | uniq  # | grep -E "www.*\/$CITY\/[A-B]{2}\/[0-9]{5}.*" -o | grep -E '[0-9]{5}' -o
elif [ "$FORMAT" = "csv" ]; then
	curl -s http://www.zipcodestogo.com/$STATE/ | grep "www.*$CITY" | grep -E '[0-9]{5}' -o | uniq | sed ':a;N;$!ba;s/\n/\,/g'
else
    usage
fi 
