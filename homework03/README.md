Homework 03
===========
Activity 1:
1. I checked if the argument is -h and displays the usage message if it 
is and takes the argument as the number of keys of it is not -h.
2. I hardcoded the lowercase.
3. I cut the source set based on the value of the key in two segments 
and combined them together reversely.
4. I use tr to replace all alphabets with the their corresponding part 
in the target set.

Activity 2:
1. I used a case statement to check whether the user specified a usage 
and set the argumennt after the usage tag as the parameter of the 
corresponding variable.
2. I used sed to remove every line that begins with the comment 
delimmiter.
3. I used sed to remove them.
4. If the command specifies a delimiter, sed will take the argument as 
its parameter and removes the line that starts with the delimiter. If 
the command specifies -W, empty lines will not be removed.

Activity 3:
1. I used a case statement to check the whether the user specified a 
usage and if he does, I set the argument after it as the parameter of my 
variable.
2. I used grep to get all strings that have 5 consecutive numbers after 
I found the state and city I was looking for.
3. I first used curl to get the data on the website of a specified state 
and then used grep to get to the specified city.
4. I first used if statement to determine which format I should use. If 
the format is text I then displayed the zip codes one line at a time. If 
the format is csv, I displayed the zip codes seperated by commas 
instead using sed.
