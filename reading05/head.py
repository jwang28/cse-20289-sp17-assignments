#!/usr/bin/env python2.7

import os
import sys

NUM = 10

def usage(status=0):
	print '''Usage: head.py files...
	
	-n  NUM      print the first NUM lines instead of the first 10'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)
	
args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
	arg = args.pop(0)
	if arg == '-n':
		NUM = int(args.pop(0))
	elif arg == '-h':
		usage(0)
	else:
		usage(1)

if len(args) == 0:
	args.append('-')

for path in args:
	if path == '-':
		stream = sys.stdin
	else:
		stream = open(path)
	
	index = 0
	for line in stream:
		line = line.rstrip()
		print line
		index += 1
		if index == NUM:
			break
	
	stream.close()	
