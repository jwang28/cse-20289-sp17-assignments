Reading 05 - Grading
========================
**Score**: 4/4

**Grader**: Mimi Chen

Deductions
-------------
(-0) Instead of "if arg in os.listdir(os.curdir)", you could just use the following line

	if os.path.exists(arg):


Comments
----------
Perfect job, Ivy :) 
