Reading 05
==========
1. 
1. import sys module (system-specific information related to Python 
interpreter).
2. It creates a for loop and loops through every argument in the command 
line.
3. The trailing , tells the program not to go to the next line every 
time after it prints out arg.

2.
1. The condition of the while loop is there are other command line 
arguments (besides the executable script itself), the first argument 
begins with the string '-', and the length of the first argument is greater than 1.
Each time the program goes through the while loop, it removes and 
returns the last item of the command line arguments.
2. If there are no other command line arguments (besides the script 
itself), the program will adds a string '-' to the argument list.
In the list of args, if the item is the string '-', the program will 
read from standard input, otherwise, it will treat the item as a 
filename and open it.
3. It strips all whitespace characters from the end of each line. It is 
necessary because lines in the files coud possibly have trailing 
whitespaces and only by stripping them can the program put a '$' right 
after the end of each line.
