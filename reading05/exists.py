#!/usr/bin/env python2.7

import sys
import os

if len(sys.argv) < 2:
	print('usage: exists.py file0...')
	exit(1)

exitcode = 0
	
for arg in sys.argv[1:]:
    if arg in os.listdir(os.curdir):
    	print('%s exists!' % arg)
    else:
    	print('%s does not exist!' % arg) 
    	exitcode = 1

exit(exitcode)
