/* ncat.c */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <string.h>

int main(int argc, char *argv[]) {
	if (argc != 3) {
		fprintf(stderr, "Usage: %s HOST PORT\n", argv[0]);
		exit(1);
	}
	struct addrinfo hints, *servinfo, *p;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	int rv;
	if ((rv = getaddrinfo(argv[1], argv[2], &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}
	int sockfd;
	for (p = servinfo; p != NULL; p = p->ai_next) {
		 if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("ncat: socket");
			continue;
		}
		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("ncat: connect");
			continue;
		}
		break;
	}
	
	if (p == NULL) {
		fprintf(stderr, "ncat: failed to connect\n");
		return 2;
	}
	char buffer[BUFSIZ];
	FILE *f = fdopen(sockfd, "w+");
	while (fgets(buffer, BUFSIZ, stdin) != NULL) {
		fprintf(f, "%s", buffer);
	}
	fclose(f);
	freeaddrinfo(servinfo);
	close(sockfd);
	return 0;
}

