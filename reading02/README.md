Reading 02
==========
1.
bash exists.sh + other arguments.
2.
chmod 755 exists.sh
3.
./exists.sh
4.
Tells Unix that the file is to be executed by bin/sh.
5.
It lists all the items in my home directory and says they exist.
6.
The first additional parameter the script was called with.
7.
Search for the existence of the file that is inputted as the first 
additional parameter.
8.
It searches for the file that is inputted as the first additional 
parameter and tells the user whether it exists or not.

