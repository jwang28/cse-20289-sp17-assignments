#!/bin/sh

if [ "$#" -eq "0" ]; then
	echo "No arguments are given."
	exit 1
fi

ERROR=0

for i in $@
do
	if [ -e "$i" ]; then
		echo "$i exists!"
	else
		echo "$i does not exist!"
		ERROR=1
	fi
done
exit $ERROR

