Reading 01
==========
1.
a. Piping the standard output of the former command into the standard 
input of the latter command.
b. Suppressing error messages from the former command.
c. Redirecting standard output to the file output.txt instead of the 
screen.
d. Comparing human readable numbers (e.g., 2K 1G).
e. No. The 1st command suppresses error messages from the command du 
while the 2nd suppresses error messages from the command sort. 
Therefore, the 2nd command generates error messages on the screen while 
the 1st does not.
2.
a. cat 2002-* > 2002
b. cat *-12 > 12
c. cat *-{01..06} | less
d. cat 200{2,4,6}-{0{1,3,5,7,9},11} | less
e. cat 200{2..4}-{09..12} | less
3.
a. Huxley, Tux
b. Huxley, Tux
c. None.
d. chmod o+rx Tux
e. chmod u-x,g-rx Tux
4.
a. Press Ctrl-z.
b. Using the fg commnand.
c. Type a Ctrl-d.
d. Press Ctrl-c.
e. Find the process id with the ps command and then terminate the 
process using kill $(pidof bc).
