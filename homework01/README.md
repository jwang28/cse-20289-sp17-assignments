Homework 01
===========
Activity 1:
1.
a. The 1st allows nd_campus and system:authuser to lookup the files 
while the 3rd allows nd_campus and system:authuser to read, lookup, and 
lock the files, and the 2nd does not grant nd_campus and 
system:authuser permissions.
b. The Private directory does not allow nd_campus and system:authuser to 
read, lookup, or lock the files while the Public directory allows them 
to do so.
2.
a. The result is "touch: cannot touch `/afs/nd.edu/web/jwang28.txt': 
Permission denied". I'm not able to create the specified file.
b. ACLs.

Activity 2:
| Command                             | Elapsed Time  |
|-------------------------------------|---------------|
| cp -r /usr/share/pixmaps images     | 2.336 seconds |
| mv images pixmaps                   | 0.006 seconds |
| mv pixmaps /tmp/jwang28-pixmaps     | 0.982 seconds |
| rm -r /tmp/jwang28-pixmaps          | 0.009 seconds |
1. 
Renaming the folder is actually copying the files into the same 
directory while moving it requires to copy the files into another 
directory, which takes more effort.
2. 
Moving requires copying the files into the new directory and deleting 
the original files, thus requires extra effort.

Activity 3:
1. 
bc < math.txt
2. 
bc < math.txt > resuts.txt
3. 
bc < math.txt > results.txt 2> /dev/null
4. 
cat math.txt | bc
Using cat and pipiline is actually executing two processes while using 
I/O redirection executes only one process.

Activity 4:
1. 
cat /etc/passwd | grep -c /sbin/nologin
2. 
who -q | uniq
3. 
du /etc 2> /dev/null | sort -n -r | head -5
4. 
ps -e | grep -c bash

Activity 5:
1.
a. q; Ctrl-c; kill PID; killall TROLL
b. kill -9
2.
a. ps ux | grep TROLL | awk '{print $2}' | head -1 | xargs kill -9
b. killall -9 TROLL
3.
killall -HUP TROLL; killall -1 TROLL
