Reading 09 - Grading
========================
**Score**: 3.75/4

**Grader**: Mimi Chen


Deductions
------------------
(-0.25) size of s is 16 bytes and size of u is 8 bytes

		s = ptr (8 bytes) + int (4 bytes) + padding (4 bytes)
		u = ptr (8 bytes)

		http://stackoverflow.com/questions/119123/why-isnt-sizeof-for-a-struct-equal-to-the-sum-of-sizeof-of-each-member



Comments
------------------
Good job, Ivy :) Have a fantastic summer!
