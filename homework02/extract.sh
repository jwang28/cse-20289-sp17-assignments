#!/bin/sh

if [ "$#" -eq "0" ]; then
	echo "Usage: extract.sh archive1 archive2..."
	exit 1
fi

while [ "$#" -gt "0" ]
do
	case "$1" in
		*.tgz)
			tar xvzf ./$1
			;;
		*.tar.gz)
			tar xvzf ./$1
			;;
		*.tbz)
			tar xvjf ./$1
			;;
		*.tar.bz2)
			tar xvjf ./$1
			;;
		*.txz)
			tar xvJf ./$1
			;;
		*.tar.xz)
			tar xvJf ./$1
			;;
		*.zip)
			unzip ./$1
			;;
		*.jar)
			unzip ./$1
			;;
	esac
	shift
done
