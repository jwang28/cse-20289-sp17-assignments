#!/bin/sh

trap catch 1 2 15

catch()
{
	cowsay "Leaving so soon?"
	exit 1
}

### main ###
cowsay "Hello, what question do you have for me today?"
read QUESTION	

while [ ! "$QUESTION" ]
do
	cowsay "What is on your mind?"
	read QUESTION
done

shuf <<'EOF' | head -1 | xargs cowsay
It is certain
It is decidedly so
Without a doubt
Yes, definitely
You may rely on it
As I see it, yes
Most likely
Outlook good
Yes
Signs point to yes
Reply hazy try again
Ask again later
Better not tell you now
Cannot predict now
Concentrate and ask again
Don't count on it
My reply is no
My sources say no
Outlook not so good
Very doubtful
EOF

