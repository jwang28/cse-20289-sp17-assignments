Homework 02
===========
Activity 1:
1. I used a if statement that tests if the number of arguments is 0. If 
it is, then I used echo to display a message and exit with error 1.
2. I used case to test if the last several characters of the 
name of the argument matches the pattern.
3. The most challenging part is to extract test archive and produced the 
messages in standard output. I overcame that by replacing gunzip with 
tar.

Activity 2:
1. I used shuf to randomly shuffle the messages and used head to get the 
first message and piped the output to cowsay.
2. I used trap to catch the signals.
3. I used read to get the standard input and stored it in a variable 
called QUESTION.
4. The most challenging part is to adjust the PATH environment variable 
to include the directory containing cowsay. My friends taught me how to 
do that but I actually get cowsay to work without using the PATH 
variable. I 

Activty 3:
1. My first step was to scan `xavier.h4x0r.space` for a HTTP port:
	$ nc -z xavier.h4x0r.space 9000-9999
	Connection to xavier.h4x0r.space 9097 port [tcp/*] succeeded!
	Connection to xavier.h4x0r.space 9111 port [tcp/*] succeeded!
	Connection to xavier.h4x0r.space 9876 port [tcp/sd] succeeded!
As can be seen, there are 3 ports in the `9000` - `9999` range.
2. Next, I tried to access the HTTP server:
	$ curl xavier.h4x0r.space:9876
	/ Halt! Who goes there?                  \
	|                                        |
	| If you seek the ORACLE, you must come  |
	| back and _request_ the DOORMAN at      |
	| /{NETID}/{PASSCODE}!                   |
	|                                        |
	| To retrieve your PASSCODE you must     |
	| first _find_ your LOCKBOX which is     |
	| located somewhere in                   |
	| ~pbui/pub/oracle/lockboxes.            |
	|                                        |
	| Once the LOCKBOX has been located, you |
	| must use your hacking skills to        |
	| _bruteforce_ the LOCKBOX program until |
	| it reveals the passcode!               |
	|                                        |
	\ Good luck!                             /
	 ----------------------------------------
3. Next, I tried to retrieve my PASSCODE:
	$ find LOCKBOX ~pbui/pub/oracle/lockboxes | grep jwang28
	$ cd ~pbui/pub/oracle/lockboxes./889c7ec1/ef305bb3/a6ed8a21/f1fa0d1f/
	$ for i in $(strings jwang28.lockbox)
		do jwang28.lockbox $i
		done
	d3c837d786234fc3aed20b058badfbdd
4. Next, I came back and requested the DOORMAN:
	$ curl xavier.h4x0r.space:9876/jwang28/d3c837d786234fc3aed20b058badfbdd
	/ Ah yes, jwang28... I've been waiting    \
	| for you.                                |
	|                                         |
	| The ORACLE looks forward to talking to  |
	| you, but you must authenticate yourself |
	| with our agent, BOBBIT, who will give   |
	| you a message for the ORACLE.           |
	|                                         |
	| He can be found hidden in plain sight   |
	| on Slack. Simply send him a direct      |
	| message in the form "!verify netid      |
	| passcode". Be sure to use your NETID    |
	| and the PASSCODE you retrieved from the |
	| LOCKBOX.                                |
	|                                         |
	| Once you have the message from BOBBIT,  |
	| proceed to port 9111 and deliver the    |
	| message to the ORACLE.                  |
	|                                         |
	| Hurry! The ORACLE is wise, but she is   |
	\ not patient!                            /
	 -----------------------------------------
5. Next, I authenticated myself with BOBBIT and get a message:
	d2puYXQyOD0xNDg2MTU3NTYy
6. Next, I proceeded to port 9111 and delivered the message to Oracle:
	$ telnet xavier.h4x0r.space 9111
The oracle told me that she hoped I learned something from this journey.
I learned a lot about using different commands and especially searching 
through directories and files to find things.
