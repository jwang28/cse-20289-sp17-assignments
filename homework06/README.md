Homework 06
===========
Activity 1:
1.My string_reverse_words first reverses the whole string and then goes through the string 
and reverses each word in the string.
The time complexity is O(n). The space complexity is O(1).
2. My string_translate first constructs a lookup table and stores all 
targets into the index corresonding to the source. It then goes through 
the string and checks if each character is initialized in the lookup 
table. If it is, it gets set to the new value.
The time complexity is O(n). The space complexity is O(1).
3. My string_to_integer takes each character of the string (starting from the rightmost) and 
finds its corresponding integer and add the value of the digit of the character.
The time complexity is O(n). The space complexity is O(1).
4. In shared libraries, all the codes related to the libraries are in the file and it is 
referenced by programs using it at run-time. While in static libraries all the codes related 
to the library is in the file and it is directly linked into the program at compile time.
libstringutils.so is larger because it includes more codes.

Activity 2:
1. The program first sets the first command line argument as the program 
name. The it goes through the command line argument list and analyze the 
char that goes after each '-', setting the mode to the corresponding 
integer. After there is no argument starts with '-', the program sets 
the next command line argument as the source, and the following as the 
target.
I uses the function translate_stream to implement translation. The 
function takes in the filestream, source, target, and mode. It then 
employs the string_translate function to translate every line of the 
stream.
I used bitmask to implement post processing filters and use if 
statements in my translate_stream to call different functions according 
to different modes. I first used enum to set all possible modes and sets 
mode when I parse command line arguments.
2. translate-static is larger because it contains all the codes it need 
to execute the program while translate-dynamic references libraries and 
not include all the codes.
translate-static works.
translate-dynamic does not work. It will require the presence of 
libraries at run-time.
