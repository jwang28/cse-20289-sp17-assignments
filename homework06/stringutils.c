/* stringutils.c: String Utilities */

#include "stringutils.h"

#include <ctype.h>
#include <string.h>
#include <stdio.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_lowercase(char *s) {
    for (char *c = s; *c; c++) {
    	*c = tolower(*c);
    }
    return s;
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_uppercase(char *s) {
    for (char *c = s; *c; c++) {
    	*c = toupper(*c);
    }
    return s;
}

/**
 * Returns whether or not the 's' string starts with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' starts with 't'
 **/
bool	string_startswith(char *s, char *t) {
    for (char *c = t; *c; c++) {
    	if (*c != *s) {
    		return false;
    	}
    	else {
    		s++;
    	}
    }
    return true;
}

/**
 * Returns whether or not the 's' string ends with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' ends with 't'
 **/
bool	string_endswith(char *s, char *t) {
	size_t tlen = strlen(t);
	for (char *c = t; *c; c++) {
	    if (*(s + strlen(s) - tlen) != *c) {
	 	   return false;
	    }
		else {
			s++;
			tlen--;
		}
	}
	return true;
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_chomp(char *s) {
    if (*(s + strlen(s) - 1) == '\n') {
    	*(s + strlen(s) - 1) = '\0'; 
    }
    return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_strip(char *s) {
    while (isspace(*s)) {
    	*s = '\0';
    	s++;
    }
    size_t slen = strlen(s);
    while (isspace(*(s + slen - 1))) {
    	*(s + slen - 1) = '\0';
    	slen--;
    }
    return s;
}

/**
 * Reverses a string given the provided from and to pointers.
 * @param   from    Beginning of string
 * @param   to      End of string
 * @return          Pointer to beginning of modified string
 **/
static char *	string_reverse_range(char *from, char *to) {
    char *c = from;
    while (c < to) {
    	char s = *c;
    	*c = *to;
    	*to = s;
    	c++;
    	to--; 
    } 
    return from;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse(char *s) {
    s = string_reverse_range(s, s + strlen(s) - 1);
    return s;
}

/**
 * Reverses all words in a string.
 * @param   s       String with words to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse_words(char *s) {
    s = string_reverse(s);
    char *c = s;
	while (*c) {
    	char *d = c;
    	while (*c && *c != ' ') {
 		   	c++;
 		}
    	d = string_reverse_range(d,c-1);
    	c++;
    } 
    return s;
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char *	string_translate(char *s, char *from, char *to) {
    char lookup[256] = {0};
    for (char *c = from; *c; c++) {
    	if (*to) {
    		lookup[(int)*c] = *to;
    		to++;
    	}
    	else {
    		break;
    	}
    }
    for (char *d = s; *d; d++) {
    	if (isalnum(lookup[(int)*d])) {
    		*d = lookup[(int)*d];
    	}
    } 
    return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	string_to_integer(char *s, int base) {
    int value = 0;
    int ref = 1;
    for (char *c = s+strlen(s)-1; c >= s; c--) {
    	int cint;
    	if (*c > 47 && *c < 58) {
    		cint = *c - 48;
    	}
    	if (*c > 64 && *c < 71) {
    		cint = *c - 55;
    	}
    	if (*c > 96 && *c < 103) {
    		cint = *c - 87;
    	}
    	value += cint * ref;
    	ref *= base;
    }
    return value;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
