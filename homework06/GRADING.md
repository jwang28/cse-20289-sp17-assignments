homework06 - Grading
===================

**Score**: 15/15

Deductions
----------

Comments
--------
Note: in string translate you should just use the char values instead of hard
coded numbers. For example, instead of seeing if a char has an ascii value
between 47 and 58, ie is a char of 0-9, just see if it is between '0' and '9'.
This makes your code much more readable.
Great work!
