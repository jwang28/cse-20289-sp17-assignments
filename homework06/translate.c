/* translate.c: string translator */

#include "stringutils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

enum {
	STRIP = 1<<1,
	RLINE = 1<<2,
	RWORD = 1<<3,
	LOWER = 1<<4,
	UPPER = 1<<5
};

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s     Strip whitespace\n");
    fprintf(stderr, "   -r     Reverse line\n");
    fprintf(stderr, "   -w     Reverse words in line\n");
    fprintf(stderr, "   -l     Convert to lowercase\n");
    fprintf(stderr, "   -u     Convert to uppercase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode) {
    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, stream)) {
    	buffer[strlen(buffer) - 1] = 0;
    	char *result = buffer; 
    	result = string_translate(result, source, target); 
    	if (mode & STRIP) {
    		result = string_strip(result); 
    	}
    	if (mode & RLINE) {
    		result = string_reverse(result);
    	}
    	if (mode & RWORD) {
    		result = string_reverse_words(result);
    	}
    	if (mode & LOWER) {
    		result = string_lowercase(result);
    	}
    	if (mode & UPPER) {
    		result = string_uppercase(result);
    	}
    	printf("%s\n", result);
    }
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */
    char *source = "";
    char *target = "";
    int mode = 0;
    int argind = 1;
    PROGRAM_NAME = argv[0];
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
    	char *arg = argv[argind++];
    	switch(arg[1]) {
    		case 'h':
    			usage(0);
    			break;
    		case 's':
    			mode |= STRIP;
    			break;
    		case 'r':
    			mode |= RLINE;
    			break;
    		case 'w':
    			mode |= RWORD;
    			break;
    		case 'l':
    			mode |= LOWER;
    			break;
    		case 'u':
    			mode |= UPPER;
    			break;
    		default:
    			usage(1);
    			break;
    	}
    }
	if (argv[argind]) {
		source = argv[argind++];
		if (argv[argind]) {
			target = argv[argind];
		}
	}

    translate_stream(stdin, source, target, mode);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
