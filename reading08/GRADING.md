Reading 08 - Grading
========================
**Score**: 2.25/4

**Grader**: Mimi Chen


Deductions
------------------
(-0.25) Q1.1 4 bytes

		int is 4 bytes

(-0.25) Q1.2 20 bytes
		
		5 ints

(-0.25) Q1.3 13 bytes

		Pointer is 8 bytes + 5 chars (need to include NUL)

(-0.25) Q1.5 8 bytes
		
		Pointer is 8 bytes

(-0.25) Q1.6 24 bytes

		Pointer is 8 bytes, and one point struct is 16 bytes

(-0.25) Q1.7 168 bytes
		
		Pointer is 8 bytes, and 10 point structs

(-0.25) Q1.8 88 bytes

		Pointer is 8 bytes, and 10 pointers to structs

Comments
------------------
