Reading 08
==========
1.
1. 8 bytes
2. 40 bytes
3. 4 bytes
4. 16 bytes
5. 0 bytes
6. 16 bytes
7. 160 bytes
8. 160 bytes

5.
1. int *randoms = malloc(n) cannot allocate memory correctly. I changed this to first 
declared randoms as a pointer to an integer array, then allocate memory of n integers 
for randoms
2. When the function finds a duplicate in random, it will return true and exit the 
function without freeing the memory allocated to random. I fixed it by clearing the 
memory space before returning a true.y


