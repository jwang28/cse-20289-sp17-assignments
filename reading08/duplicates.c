/* duplicates.c */
//#include <stdio.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int NITEMS = 1<<10;
const int TRIALS = 100;

bool contains(int *a, int n, int k) {
    /* TODO: Search through array to see if there is an element that matches key */
    for (int i = 0; i < n; i++) {
    	if (a[i] == k) {
    		return true;
    	}
    }
    return false;
}

bool duplicates(int n) {
    int *randoms;
    randoms = malloc(sizeof(int) * n);

    for (int i = 0; i < n; i++) {
    	randoms[i] = rand() % 1000;
    }

    for (int i = 0; i < n; i++) {
        /* TODO: Use contains to search array for duplicate */ 
    	int num = randoms[i];
    	if (contains(randoms+i+1, n-i-1, num)) {
    	    free(randoms);
    	    return true;
        }
    }

    free(randoms);
    return false;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    for (int i = 0; i < TRIALS; i++)
    	if (duplicates(NITEMS))
	    puts("Duplicates detected!");
	    
    return 0;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
