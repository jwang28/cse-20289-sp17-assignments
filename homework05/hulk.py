#!/usr/bin/env python2.7

import functools
import hashlib
import itertools
import multiprocessing
import os
import string
import sys

# Constants

ALPHABET    = string.ascii_lowercase + string.digits
ARGUMENTS   = sys.argv[1:]
CORES       = 1
HASHES      = 'hashes.txt'
LENGTH      = 1
PREFIX      = ''

# Functions

def usage(exit_code=0):
    print '''Usage: {} [-a alphabet -c CORES -l LENGTH -p PATH -s HASHES]
    -a ALPHABET Alphabet to use in permutations
    -c CORES    CPU Cores to use
    -l LENGTH   Length of permutations
    -p PREFIX   Prefix for all permutations
    -s HASHES   Path of hashes file'''.format(os.path.basename(sys.argv[0]))
    sys.exit(exit_code)

def md5sum(s):
    ''' Generate MD5 digest for given string.

    >>> md5sum('abc')
    '900150983cd24fb0d6963f7d28e17f72'

    >>> md5sum('wake me up inside')
    '223a947ce000ce88e263a4357cca2b4b'
    '''
    # TODO: Implement
    return hashlib.md5(s).hexdigest()

def permutations(length, alphabet=ALPHABET):
    ''' Yield all permutations of alphabet up to provided length.

    >>> list(permutations(1, 'ab'))
    ['a', 'b']

    >>> list(permutations(2, 'ab'))
    ['aa', 'ab', 'ba', 'bb']

    >>> list(permutations(1))       # doctest: +ELLIPSIS
    ['a', 'b', ..., '9']

    >>> list(permutations(2))       # doctest: +ELLIPSIS
    ['aa', 'ab', ..., '99']
    '''
    # TODO: Implement
    pass
    if length > 1:
    	for i in alphabet:
    		for j in permutations(length-1, alphabet):
	    		yield i + j
    else:
    	for i in alphabet:
    		yield i

def smash(hashes, length, alphabet=ALPHABET, prefix=''):
    ''' Return all password permutations of specified length that are in hashes

    >>> smash([md5sum('ab')], 2)
    ['ab']

    >>> smash([md5sum('abc')], 2, prefix='a')
    ['abc']

    >>> smash(map(md5sum, 'abc'), 1, 'abc')
    ['a', 'b', 'c']
    '''
    # TODO: Implement
    pass
    return [ prefix + x for x in permutations(length, alphabet) if md5sum(prefix + x) in hashes ]

# Main Execution

if __name__ == '__main__':
    # TODO: Parse command line arguments
    pass
    args = sys.argv[1:]
    while len(args) and args[0].startswith('-') and len(args[0]) > 1:
		arg = args.pop(0)
		if arg == '-a':
			ALPHEBET = args.pop(0)
		if arg == '-c':
			CORES = int(args.pop(0))
		if arg == '-l':
			LENGTH = int(args.pop(0))
		if arg == '-p':
			PREFIX = args.pop(0)
		if arg == '-s':
			HASHES = args.pop(0)
		if arg == '-h':
			usage()
		
    # TODO: Load hashes set
    pass
    hashset = set()
    for word in open(HASHES):
		hashset.add(word.rstrip())
		
    # TODO: Execute smash function to get passwords
    pass
    if CORES > 1 and LENGTH > 1:
    	prefixlength = LENGTH / 2
    	restlength = LENGTH - prefixlength
    	pool = multiprocessing.Pool(CORES)
    	submash = functools.partial(smash, hashset, restlength, ALPHABET)
    	passwords = itertools.chain.from_iterable(pool.imap(submash, ( PREFIX + p for p in permutations(prefixlength, ALPHABET) )))
    else:
		passwords = smash(hashset, LENGTH, ALPHABET, PREFIX)
    # TODO: Print passwords
    pass
    for password in passwords:
    	print password

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
 
