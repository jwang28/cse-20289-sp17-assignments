Homework 05
===========
Activity 1:
1.
generate all candidate password combinations:
I used recursions to add all possible alphabets to the front of all the 
element in the list each time until it reaches the desired length.

filter the candidates to only contain valid passwords:
I used list comprehensions to add the prefix to all the permutations I 
got and see if generated MD5 digest is in the given set of hashes.

handle processing on multiple cores:
If the number of cores is greater than 1 and the lenght is also greater 
than 1, I used functools.partial to seperate the alphabets into 2 
parts and process them seperately. I used multiprocessing.Pool to employ multiple cores to run the 
program at the same time. 

verify that my code works properly:
I tested my code with both test_hulk.sh and doctests.

2.
1: 263m36.856s
2: 214m51.810s
4: 107m32.774s
8: 51m24.016s
16: 64m29.139s

3.
A longer password would be more difficult to brute-force since the 
possible permutations increase drastically as the length grows.

Activity 2:
iv_map.py
keep track of line numbers:
initialize linenum at the very beginning and increment it every time it 
goes through the for loop.

remove undesirable characters:
I used list comprehensions and concatenate all chars that are either an 
alphabet or a '-' in each word.

iv_reduce.py:
aggregate the results for each word:
use a set to store the words and add the line number to the 
corresponding word each time it encounters a word

output the results in the correct format:
I used map to convert all strings into integers and join them together 
with a space in between.
