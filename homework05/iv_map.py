#!/usr/bin/env python2.7

import sys

linenum = 0

for line in sys.stdin:
	linenum += 1
	for word in line.strip().split():
		lowerword = ''.join([ letter.lower() for letter in word if letter.isalpha() or letter == '-' ])
		print '{}\t{}'.format(lowerword, linenum)
