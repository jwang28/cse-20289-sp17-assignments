Reading 03
==========
1. echo "All your base are belong to us" | tr a-z A-Z
2. echo "monkeys love bananas" | sed 's/monkeys/gorillaz/'
3. echo "     monkeys love bananas" | sed 's/^[[:space:]]*//'
4. cat /etc/passwd | grep '^root' | cut -d: -f7
5. cat /etc/passwd | sed -r 's_\/bin\/(bash|csh|tcsh)_\/usr\/bin\/python_' | grep python 
6. cat /etc/passwd | grep -E '4[0-9]{0,}7'
7. comm -12 file1.txt file2.txt
8. diff file1.txt file2.txt
