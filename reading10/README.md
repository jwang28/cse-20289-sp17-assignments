Reading 10
==========
1.
1. perror("exec failed!");
2. ftruncate(int fd, off_t length);
3. fseek(file, 10, SEEK_SET);
4. struct stat statbuf;
   if (stat(path, &statbuf) != 0) return 0;
   S_ISDIR(statbuf.st_mode);
5. fork();
6. exec();
7. exit();
8. waitpid(child, &status, 0);
