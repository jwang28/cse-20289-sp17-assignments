/* walk.c */

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

int main() {
	DIR *pDir;
	struct dirent *pDirent;
	pDir = opendir(".");
	if (pDir) {
		struct stat sb;
		while ((pDirent = readdir(pDir)) != NULL) {
			stat(pDirent->d_name, &sb);
			if (S_ISREG(sb.st_mode)) {
				printf("%s %d\n", pDirent->d_name, (int)sb.st_size);
			}
		}
		closedir(pDir);
	}
}
