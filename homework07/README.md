Homework 07
===========
Activity 1:
1.
In my node_create, I use malloc to allocate a memory chunck which has the size of the 
struct node and also allocate memory for the string.
In my node_delete, I use free to free the memory take up by the node and the string.
If recursive is true, I continue moving the pointer to the next node until it reaches 
NULL.
2.
In my list_create, I use calloc to allocate a memory chunck which has the size of a 
struct list.
In my list_delete, I use free to free the memory taken up by the struct and also calls 
node_delete function to free the memory taken up by the nodes recursively.
3.
My list_qsort converts the list to an array and sorts the array using the function qsort 
then links the node in the list according to the new order.
time complexity: average O(n) worst O(n^2)
space complexity: average O(n) worst O(n)
4.
My list_reverse sets the tail of the list as the head of the list and sets the head of 
the list by calling the recursive function reverse.
time complexity: average O(n) worst O(n)
space complexity: average O(1) worst O(1)
4.
My list_msort sets the head of the list by calling the recursive function msort and 
links the nodes in the list according to the new order.
time complexity: average O(logn) worst O(logn)
space complexity: average O(1) worst O(1)
 
Activity 2:
| NITEMS   | SORT     | TIME     | SPACE    |
| -------- | -------- |----------|----------|
| 1        | Merge    |
0.000999 seconds        0.472656 Mbytes
| 1        | Quick    |
0.000999 seconds        0.480469 Mbytes
| 10       | Merge    |
0.000999 seconds        0.472656 Mbytes
| 10       | Quick    |
0.002999 seconds        0.476562 Mbytes
| 100      | Merge    |
0.000999 seconds        0.476562 Mbytes
| 100      | Quick    |
0.000999 seconds        0.484375 Mbytes
| 1000     | Merge    |
0.000999 seconds        0.531250 Mbytes
| 1000     | Quick    |
0.000999 seconds        0.621094 Mbytes
| 10000    | Merge    |
0.007997 seconds        1.078125 Mbytes
| 10000    | Quick    |
0.006998 seconds        1.289062 Mbytes
| 100000   | Merge    |
0.103984 seconds        6.574219 Mbytes
| 100000   | Quick    |
0.089985 seconds        8.160156 Mbytes
| 1000000  | Merge    |
1.945703 seconds        61.507812 Mbytes
| 1000000  | Quick    |
1.346794 seconds        77.750000 Mbytes
| 10000000 | Merge    |
24.586262 seconds       610.824219 Mbytes
| 10000000 | Quick    |
17.329365 seconds       763.898438 Mbytes

1.
As the number of items increase, it takes more time and more space to sort using both 
methods. The results are not surprising because there are more operations need to be 
performed when there are more items.
2.
Theoretical complexity measures the growth tendency. It can only show yout the 
rising trend of the time and space, but not the actual performance of the program.
