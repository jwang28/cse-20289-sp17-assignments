Homework 07 - Grading
====================

**Score**: 14/15

Deductions
----------
README
-.25 list_qsort average time complexity should be O(nlog(n))
-.25 list_reverse space complexity should be O(n)
-.25 list_msort time complexity should be O(nlog(n)) and space should be O(log(n))

benchmark
-.25 benchmark.py does not run correctly on student00

Comments
--------
Great job!
