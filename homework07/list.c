/* list.c */

#include "list.h"

/* Internal Function Prototypes */

static struct node*	reverse(struct node *curr, struct node *next);
static struct node *	msort(struct node *head, node_compare f);
static void		split(struct node *head, struct node **left, struct node **right);
static struct node *	merge(struct node *left, struct node *right, node_compare f);

/**
 * Create list.
 *
 * This allocates a list that the user must later deallocate.
 * @return  Allocated list structure.
 */
struct list *	list_create() {
    struct list *l = calloc(1, sizeof(struct list));
    return l;
}

/**
 * Delete list.
 *
 * This deallocates the given list along with the nodes inside the list.
 * @param   l	    List to deallocate.
 * @return  NULL pointer.
 */
struct list *	list_delete(struct list *l) {
    node_delete(l->head, 1);
    free(l);
    return NULL;
}

/**
 * Push front.
 *
 * This adds a new node containing the given string to the front of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_front(struct list *l, char *s) {
	struct node *p = node_create(s, l->head);
	l->head = p;
	if (l->size == 0) {
		l->tail = p;
	}
	l->size ++;
}

/**
 * Push back.
 *
 * This adds a new node containing the given string to the back of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_back(struct list *l, char *s) {
	struct node *p = node_create(s, NULL);
	if (l->size == 0) {
		l->head = p;
		l->tail = p;
	}
	else {
		l->tail->next = p;
		l->tail = p;
	}
	l->size ++;
}

/**
 * Dump list to stream.
 *
 * This dumps out all the nodes in the list to the given stream.
 * @param   l	    List structure.
 * @param   stream  File stream.
 */
void		list_dump(struct list *l, FILE *stream) {
	struct node *p = l->head;
	while (p) {
		node_dump(p, stream);
		p = p->next;
	}
}

/**
 * Convert list to array.
 *
 * This copies the pointers to nodes in the list to a newly allocate array that
 * the user must later deallocate.
 * @param   l	    List structure.
 * @return  Allocate array of pointers to nodes.
 */
struct node **	list_to_array(struct list *l) {
    struct node **a = malloc(sizeof(struct node) * l->size);
    struct node *p = l->head;
    int i = 0;
    while (p) {
    	a[i] = p;
    	p = p->next;
    	i++;
    }
    return a;
}

/**
 * Sort list using qsort.
 *
 * This sorts the list using the qsort function from the standard C library.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_qsort(struct list *l, node_compare f) {
	struct node **a = list_to_array(l);
	qsort(a, l->size, sizeof(struct node*), f);
	l->head = a[0];
	for (int i = 0; i < l->size - 1; i++) {
		a[i]->next = a[i+1];
	}
	l->tail = a[l->size - 1];
	l->tail->next = NULL;
	free(a);
}

/**
 * Reverse list.
 *
 * This reverses the list.
 * @param   l	    List structure.
 */
void		list_reverse(struct list *l) {
	l->tail = l->head;
	l->head = reverse(l->head, NULL);
}

/**
 * Reverse node.
 *
 * This internal function recursively reverses the node.
 * @param   curr    The current node.
 * @param   prev    The previous node.
 * @return  The new head of the singly-linked list.
 */
struct node*	reverse(struct node *curr, struct node *prev) {
    struct node *tail = curr;
    if (curr->next) {
    	tail = reverse(curr->next, curr);
    }
    curr->next = prev;
    return tail;
}



/**
 * Sort list using merge sort.
 *
 * This sorts the list using a custom implementation of merge sort.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_msort(struct list *l, node_compare f) {
	l->head = msort(l->head, f);
	struct node *p = l->head;
	while (p->next) {
		p = p->next;
	}
	l->tail = p;
}

/**
 * Performs recursive merge sort.
 *
 * This internal function performs a recursive merge sort on the singly-linked
 * list starting with head.
 * @param   head    The first node in a singly-linked list.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	msort(struct node *head, node_compare f) {
    struct node *left = head;
    struct node *right = head;
    if (head == NULL || head->next == NULL) {
    	return head;
    }
    split(head, &left, &right);
    left = msort(left, f);
    right = msort(right, f);
    return  merge(left, right, f);
}

/**
 * Splits the list.
 *
 * This internal function splits the singly-linked list starting with head into
 * left and right sublists.
 * @param   head    The first node in a singly-linked list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 */
void		split(struct node *head, struct node **left, struct node **right) {
	struct node *p = head;
	struct node *q = head;
	struct node *prev = head;
	while (p && p->next) {
		p = p->next->next;
		prev = q;
		q = q->next;
	}
	if (prev) {
		prev->next = NULL;
	}
	*right = q;
	*left = head;
}

/**
 * Merge sublists.
 *
 * This internal function merges the left and right sublists into one ordered
 * list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	merge(struct node *left, struct node *right, node_compare f) {
    struct node *p = left;
    struct node *q = right;
    struct node head = {NULL, 0, NULL};
    struct node *tail = &head;
    while (p && q) {
    	if (f(&p, &q) <= 0) {
    		tail->next = p;
    		tail = tail->next;
    		p = p->next;
    	}
    	else {
    		tail->next = q;
    		tail = tail->next;
    		q = q->next;
    	}
    }
    while (p) {
    	tail->next = p;
    	tail = tail->next;
    	p = p->next;
    }
    while (q) {
    	tail->next = q;
    	tail = tail->next;
    	q = q->next;
    }
    tail->next = NULL;
    return head.next;
}
