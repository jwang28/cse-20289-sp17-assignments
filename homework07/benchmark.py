#!/usr/bin/env python2.7
import os
import subprocess

print '| NITEMS   | SORT     | TIME     | SPACE    |'
print '| -------- | -------- |----------|----------|'
num = 1
while (num <= 10000000):
	print '| {:<8} | Merge    |'.format(num)
	os.popen('shuf -i1-{} -n {} | ./measure ./lsort -n > /dev/null'.format(num, num))
	print '| {:<8} | Quick    |'.format(num)	
	os.popen('shuf -i1-{} -n {} | ./measure ./lsort -n -q > /dev/null'.format(num, num))
	num *= 10
	

