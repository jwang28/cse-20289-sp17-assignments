#!/usr/bin/env python2.7

import atexit
import os
import re
import shutil
import sys
import tempfile

import requests
import urllib

os.environ['PATH'] = '~ccl/software/external/imagemagick/bin:' + os.environ['PATH']

# Global variables

REVERSE     = False
DELAY       = 20
STEPSIZE    = 5

# Functions

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    # TODO: Parse command line arguments
    if arg == '-r':
    	REVERSE = True
    elif arg == '-d':
    	DELAY = int(args.pop(0))
    elif arg == '-s':
    	STEPSIZE = int(args.pop(0))

if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]

# Main execution

# TODO: Create workspace
location = tempfile.mkdtemp()
print 'Using workspace: {}'.format(location)

# TODO: Register cleanup
def cleanup(dir):
	shutil.rmtree(dir)
	print 'Cleaning up workspace: {}'.format(dir)
	
atexit.register(cleanup, location)

# TODO: Extract portrait URLs
URL = 'http://engineering.nd.edu/profiles/'
print 'Searching portrait for {}...'.format(netid1),
try:
	r1 = requests.get(URL+netid1)
	result1 = re.findall('(http://engineering.nd.edu/profiles/.*.jpeg)', r1.text)
	img1 = result1[0]
	print img1
except:
	print 'Not Found!'
	sys.exit(1)
print 'Searching portrait for {}...'.format(netid2),
try:
	r2 = requests.get(URL+netid2)
	result2 = re.findall('(http://engineering.nd.edu/profiles/.*.jpeg)', r2.text)
	img2 = result2[0]
	print img2
except:
	print 'Not Found!'
	sys.exit(1)

# TODO: Download portraits
path1 = os.path.join(location, netid1 + '.jpg')
path2 = os.path.join(location, netid2 + '.jpg')
urllib.urlretrieve(img1, path1)
urllib.urlretrieve(img2, path2)
print 'Downloading {} to {}'.format(img1, path1)
print 'Downloading {} to {}'.format(img2, path2)

# TODO: Generate blended composite images
index = 0
names = ''
while index <= 100:
	name = location + '/' + str(index) + '-blend.gif'
	os.system('composite -blend {} {} {} {}'.format(index, path1, path2, name))
	print 'Generating {}/{} ... Success!'.format(location, name)
	index += STEPSIZE
	names = names + name + ' '	

# TODO: Generate final animation
if REVERSE:
	index = 100
	while index >= 0:
		name = location + '/' + str(index) + '-blend.gif'
		names = names + name + ' '
		index -= STEPSIZE

os.system('convert -loop 0 -delay {} {} {}'.format(DELAY, names, target))
print 'Generating {} ... Success!'.format(target)
