Homework 04
===========
Activity 1:
1. I checked if the first argument in the list args has a special tag, 
if it is, I set the corresponding value to the next argument in the 
list. Then I removed every argument I checked.
2. I created a temporary folder and deleted the folder every time the 
script finished running.
3. I first get the contents of the profile page of the person the netid 
corresponds to and then search through to find the URL of the image.
4. I use urlretrieve and takes the URL of the image and the path of the 
filename that I want to write it to save the image to the file.
5. I use blend to take both images and blend them together according to 
the different stepsizes.
6. I use convert and takes all the blended gifs I get as input 
arguments and automate the transition.
7. I used try and except to catch errors and exit the execution.

Activity 2:
1. I checked if the first element in the list of argumentss has one of the 
tags I am looking for, if it does, I set the corresponding value to the next 
element in the list. Then I removed the argument I checked.
2. I used requests.get to find the json data and then I get into the dictionary 
that contains the infomation of each article and used a while loop to go 
through the articles to display the title, author, URL of each article 
that meets the requirement until I get enough number of outputs.
3. I used re.search and take the values of field and regex as my inputs.

