#!/usr/bin/env python2.7

import re
import os
import sys
import requests

FIELD = 'title'
LIMIT = 10
SUBREDDIT = 'linux'
regex = ''

def usage(status = 0):
	print '''Usage: reddit.py [ -f FIELD -s SUBREDDIT ] regex
	-f FIELD	Which field to search (default: title)
	-n LIMIT	Limit number of articles to report (default: 10)
	-s SUBREDDIT    Which subreddit to search (default: linux)'''
	sys.exit(status)
	
args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
	arg = args.pop(0)
	if arg == '-h':
		usage()
	elif arg == '-f':
		FIELD = args.pop(0)
	elif arg == '-n':
		LIMIT = int(args.pop(0))
	elif arg == '-s':
		SUBREDDIT = args.pop(0)

if len(args) > 1:
	usage(1)
elif len(args) == 1:
	regex = args[0]

URL = 'https://www.reddit.com/r/{}/.json'.format(SUBREDDIT)
headers  = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}
response = requests.get(URL, headers=headers)
articles = response.json()['data']['children']

index = 0
find = 0
while find < LIMIT:
	content = articles[index]['data']
#	print str(content[FIELD])
#	break
	try:
		match = re.search(regex, str(content[FIELD]))
		if match:
			print ' {}.	Title:	{}'.format(index+1, content['title'])
			print '	Author:	{}'.format(content['author'])
			print '	Link:	{}'.format(content['url'])
			print ''
			find +=1
		index += 1
	except:
		print 'Invalid field: {}'.format(FIELD)
		sys.exit(1)


