#!/bin/sh

NUM=5
DECEND=0

usage() {
	cat <<EOF
Usage: rank.sh [ -n N -D]

	-n N 	Returns N items (default is 5).
	-D 	Rank in descending order.
EOF
	exit $1
}

while [ $# -gt 0 ]; do
	case $1 in
	-n) NUM=$2 shift;;
	-D)	DECEND=1;;
	-h) usage 0;;
	*)  usage 1;;
	esac
	shift
done

if [ $DECEND -eq 1 ]; then
	sort -gr | head -n $NUM
else
	sort -g | head -n $NUM
fi
