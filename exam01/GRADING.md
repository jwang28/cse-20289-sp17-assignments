GRADING - Exam 01
=================

- Commands:         1.75
- Short Answers:
    - Files:        3 
    - Processes:    3 
    - Pipelines:    3
- Debugging:        3.75
- Code Evaluation:  2.5 
- Filters:          3 
- Scripting:        3 
- Total:	    23.0
