## GRADING
### Reading07
##### Deductions
README.md Q2 -- No, this would not work size sizeof returns how much memory a variable or data type uses.  In C, arrays 
are actually pointers, so sizeof(numbers) simply returns the amount of data used to store the base pointer (8 bytes) 
rather than the number of elements.
---
##### Final Grade: 3.75/4
---
*Graded by Maggie Thomann*
