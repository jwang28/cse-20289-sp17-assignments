Reading 07
==========
1.
The function reads the integers from stdin and count the number of 
inputs.
The condition of the while loop is both i less than n and the end of the 
file has not been reached.
the & in front of the numbers[i] represents a pointer to the element at 
index i.
EOF refers to the end of the file (in this case, standard input). It is 
a negative integer, normally -1.
2.
Yes. It works because the sizeof function returns the length of the 
array, which is the same value as n.

1.
The conditions they check in the while loop is the same. They both go 
through the command line arguments one by one and process each of them 
and then move on to the next one.
The difference is that cat.c keeps track of the number of command line 
arguments using argc (which is an integer represents the total number of 
command line arguments) and goes through the elements in argv (which is 
a 2d array of chars storing all command line arguments). cat.py always 
checks the first argument in the list and deletes it after checking it.
2.
As long as the program hasn't reached the end of the command line 
arguments, it stores the corresponding argument into a string called 
path. If the string stored in path is the same as '-', the program will 
process standard input, otherwise it will process the file with the name 
of the path.
3.
It first creates an array of chars with size BUFSIZ called buffer, and 
it reads in all characters from stream and store them in buffer using 
fgets, it then write the string stored in buffer to standard out.
4.
It first opens the file specified by string stored in path and then 
checks if the string file is empty (cannot find/open the file). If it 
is, it will output an error message. Otherwise, it will use the function 
cat_stream to read and write the content of the file to standard output. 
it then closes the file.
The if statement checks if the file cannot be found or opened. stderror 
returns a pointer to a string that describes the error code passed in 
the argument errno (which is the number of the last error).

