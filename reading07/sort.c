/* sort.c */

#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define MAX_NUMBERS (1<<10)

/* Functions */

size_t read_numbers(FILE *stream, int numbers[], size_t n) {
    size_t i = 0;

    while (i < n && scanf("%d", &numbers[i]) != EOF) {
        i++;
    }

    return i;
}

void sort_numbers(int numbers[], size_t n) {
    for (size_t i = 1; i < n; i++) {
        size_t j = i;
        while (j > 0 && numbers[j-1] > numbers[j]) {
        	int k = numbers[j];
        	numbers[j] = numbers[j-1];
        	numbers[j-1] = k;
        	j--;
        }
    }
   	for (size_t i = 0; i < n-1; i++) {
    	printf("%d ", numbers[i]);
    }
    printf("%d", numbers[n-1]);
    printf("\n");
}

/* Main Execution */

int main(int argc, char *argv[]) {
    int numbers[MAX_NUMBERS];
    size_t nsize;

    nsize = read_numbers(stdin, numbers, MAX_NUMBERS);
    if (nsize > 0) sort_numbers(numbers, nsize);
	else printf("\n");
    return EXIT_SUCCESS;
}
