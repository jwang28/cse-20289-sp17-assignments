/* grep.c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s PATTERN FILE...\n", PROGRAM_NAME);
    exit(status);
}

void grep_stream(FILE *stream, const char *pattern) {
    char buffer[BUFSIZ];
    char *substring;
	while (fgets(buffer, BUFSIZ, stream)) {
    	substring = strstr(buffer, pattern);
    	if (substring != NULL) {
    		fputs(buffer, stdout);
    	}
    }
}

void grep_file(const char *path, const char *pattern) {
    FILE *fs = fopen(path, "r");
    if (fs == NULL) {
        fprintf(stderr, "%s: %s: %s\n", PROGRAM_NAME, path, strerror(errno));
        return;
    }
	grep_stream(fs, pattern);
    fclose(fs);
}

/* Main Execution */

int main(int argc, char *argv[]) {
    int argind = 1;

    /* Parse command line arguments */
    PROGRAM_NAME = argv[0];
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            default:
                usage(1);
                break;
        }
    }

    /* Process file */
    if (argind + 1 == argc) {
        char *pattern = argv[argind];
     	grep_stream(stdin, pattern);
    } else if (argind + 2 == argc) {
        char *pattern = argv[argind++];
        char *path = argv[argind++];
        if (strcmp(path, "-") == 0) {
            grep_stream(stdin, pattern);
        } else {
            grep_file(path, pattern);
        }
    }

    return EXIT_SUCCESS;
}
