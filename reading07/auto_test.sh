#!/bin/bash
# Author: Margaret Thomann
# Grader utility for testing reading07
echo "------------------TESTING SORT----------------"
./test_sort.sh
echo "------------------TESTING GREP----------------"
./test_grep.sh
echo "------------------TESTING MAKEFILE----------------"
make clean
make
make test
