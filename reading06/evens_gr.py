#!/usr/bin/env python2.7

import sys

def evens(stream):
    for i in stream:
    	if int(i.strip()) % 2 == 0:
    		yield i.strip()
    
print ' '.join(evens(sys.stdin))
