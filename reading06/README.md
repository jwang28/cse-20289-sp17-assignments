Reading 06
==========
2.
A. MapReduce is trying to solve the problem that when implementing 
special-purpose computations that process large amounts of raw data 
especially when the input data is large and the computations have to be distributed across hundreds or 
thousands of machines in order to finish in a reasonable amount of time, 
parallelizing the computation, distributing the data, and handling 
failures obscure the original simple computation.
B. Map phase: data in each input split is passed to a mapping function 
to produce output values.
   Shuffle phase: consolidate the relevant records from map phase 
output.
   Reduce phase: combines values from shuffle phase and returns a single 
output value.
